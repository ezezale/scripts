function System(choices,size){
  this.choices = choices;
  this.indexLearning = 2;    
  this.nodes = [];  
  var sz = 0;
  for(var j=0;j<this.choices.length;j++){
    sz += (this.choices[j].length-1).toString(2).length;
  }
  size = Math.floor(size / sz)*sz;
  for(var i=0;i<size;i++){
      n = new Node(this.indexLearning);
      this.nodes.push(n);
  }
}
System.prototype.divideHashCodeToInts = function(s) {
    var hash = 0, i,k=0;
    var hashes = [];
     for (i = 0; i < s.length; i++) {
        hashes.push(s.charCodeAt(i));
    }
   /* for (i = 0; i < s.length; i++) {
        hash = s.charCodeAt(i)*i;
    }
    return [hash];
   allChars = "0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    for (i = 0; i < s.length; i++) {
        hash += allChars.indexOf(s.charAt(i))*Math.pow(allChars.length,k++);
        if (i!=0 && i % 5 == 0){
            hashes.push(hash);///Math.pow(10,hashes.length));
            hash = 0;
            k = 0;
        }
    }
    if (hash > 0){
        hashes.push(hash);
    }*/
   return hashes;
};
System.prototype.normalize = function(data){
    for (var i=0;i < data.length; i++){
        if (typeof(data[i]) == 'string'){
            if (!isNaN(data[i])){
                data[i] = parseFloat(data[i]);
            }else{
                var hashs = this.divideHashCodeToInts(data[i]);
                data[i] = hashs[0];
                for (var j=1;j<hashs.length;j++){
                    data.push(hashs[j]);
                }
            }
        }
    }
    return data;
};
System.prototype.wasLearn = function(){
  return this.nodes[0].wasLearn();
};
System.prototype.learn = function(data,result){
  var i, j, k, n,output,choiceOutput;
  data = this.normalize(data);    
  n=0;
  for (i=0;i < this.nodes.length; i=n){
    for (j=0; j<this.choices.length;j++){
      choiceOutput = (this.choices[j].length-1).toString(2).length;
      output = this.choices[j].indexOf(result[j]).toString(2);
      for (k=choiceOutput-1;k > -1; k--){
        if (k < output.length){
          this.nodes[n].learnFromData(data,parseInt(output[k]));
        }else{
          this.nodes[n].learnFromData(data,0);
        }    
        n++;
      }
    }
  }
};
System.prototype.predict = function(data){
  var i,j,k,n,sortable,output,choiceOutput,result,predicted;
  if (this.wasLearn()){
    result = [];
    for (j=0; j<this.choices.length;j++){
      result.push({});
    }
    data = this.normalize(data);    
    n=0;
    for (i=0;i < this.nodes.length; i=n){
      for (j=0; j<this.choices.length;j++){
        choiceOutput = (this.choices[j].length-1).toString(2).length;
        output = '';
        for (k=choiceOutput-1;k > -1; k--){
          output += this.nodes[n].predict(data);
          n++;
        }
        output = parseInt(output,2);
        if (this.choices[j].length > output){          
          if (result[j][this.choices[j][output]] === undefined){
            result[j][this.choices[j][output]] = 1;
          }else{
            result[j][this.choices[j][output]] += 1;
          }
        }else{
          //result[n].push(null);
        }
      }
    }
    predicted = [];
    sortable = [];
    for (j=0; j<this.choices.length;j++){
      sortable.push([]);
      for (var r in result[j]) {
        sortable[j].push([r, result[j][r]]);
      }
      sortable[j].sort(function(a, b) {
          return b[1] - a[1];
      });
      predicted.push(sortable[j][0]);
    }        
    return predicted;
  }else{
      console.log("You must Learn Before Predict");
  }
  return null;
};

function Node(indexLearning){
    this.learningIndex = indexLearning;
    this.limitMax = null;
    this.limitMin = null;    
}
Node.prototype.wasLearn = function(){
  return this.limitMax != null;
};
Node.prototype.learnFromData = function(data,result){
  var i;
  if (!this.wasLearn()){
    this.limitMax = [];
    this.limitMin = []; 
    for (i=0; i<data.length;i++){
      if (result === 1){
        this.limitMax.push(data[i]+data[i]*Math.random());
        this.limitMin.push(data[i]-data[i]*Math.random());
      }else{
        this.limitMax.push(0);
        this.limitMin.push(0);
      }
    }
  }else{
    for (i=0; i<data.length;i++){
      //In Interval
      if (data[i] <= this.limitMax[i] && data[i] >= this.limitMin[i]){
        if ( result == 1){
          this.limitMax[i] = (this.limitMax[i] * this.learningIndex + data[i])/(this.learningIndex+1);
          this.limitMin[i] = (this.limitMin[i] * this.learningIndex + data[i])/(this.learningIndex+1);
        }
      }else{
        if (result == 1){
          if (this.limitMax[i] < data[i]){
            this.limitMax[i] = (data[i] - this.limitMax[i])*this.learningIndex + this.limitMax[i];  
          }else{
            this.limitMin[i] = this.limitMin[i] - (this.limitMin[i]-data[i])*this.learningIndex;  
          }
        }
      }
    }
  }
};

Node.prototype.predict = function(data){
    var i, OKData = 0;
    if (this.wasLearn()){
        for (i=0; i<data.length;i++){
            if (data[i] <= this.limitMax[i] && data[i] >= this.limitMin[i]){
              OKData++;
            }          
        }
        return OKData/data.length > 0.5? 1:0;
    }else{
        console.log("You must Learn Before Predict");
    }
    return 0;
};
